import json, logging, math, pika, uuid

__all__ = ['RMQUtils', 'NSSIService', 'NSSIConsumer']

class RMQUtils():
    """A class to manage connecting to, consuming from, and publishing to RMQ"""

    __connection = None
    __channel = None
    __retry_queues = []
   
    def __init__(self):
        pass 

    def connect(self, params={}):
        """Establishes a connection to an RMQ instance with the given parameters."""

        credentials = pika.PlainCredentials(params.get('user', 'guest'), params.get('password', 'guest'))
        connection_params = pika.ConnectionParameters(params.get('host', 'localhost'),
                                                      params.get('port', 5672),
                                                      params.get('virtual_host', '/'),
                                                      credentials)

        self.__connection = pika.BlockingConnection(connection_params)
        self.__channel = self.__connection.channel()

    def declare_queue_and_listen(self, queue_name, callback):
        """Declares the given queue and binds it to the appMessagesExchange. The provided callback is
           attached to the new queue."""

        if not self.__channel:
            return

        self.__channel.queue_declare(queue=queue_name,
                                     passive=False,
                                     durable=True,
                                     exclusive=False,
                                     auto_delete=False,
                                     arguments={'x-dead-letter-exchange': 'appMessages.dlx'})
        self.__channel.queue_bind(exchange='appMessagesExchange', queue=queue_name, routing_key=queue_name)
        self.__channel.basic_consume(queue=queue_name, auto_ack=True, on_message_callback=callback)

    def declare_retry_queue(self, queue_name, ttl):
        """Declares the given retry queue, with the given time-to-live. Retry queues have no consumers;
           failed messages will sit on the queue for the time-to-live specified, after which they will
           timeout and be retried by NSSI."""

        if not self.__channel:
            return

        self.__channel.queue_declare(queue=queue_name,
                                     passive=False,
                                     durable=True,
                                     exclusive=False,
                                     auto_delete=False,
                                     arguments={'x-dead-letter-exchange': 'appMessages.dlx', 'x-message-ttl': ttl})
        self.__retry_queues.append(queue_name)

    def listen(self):
        self.__channel.start_consuming()

    def send_to_next_step(self, serviceName, message):
        """Forwards the given message to the next processing step, according to the value of its pipeline
           property. This property is set by NSSI and should not be modified by the service itself. If there
           are no more steps, a progress update is sent to NSSI to indicate that the processing is complete."""

        pipeline = message.get('pipeline', [])
        if not pipeline:
            self.send_progress_message({'jobId': message['jobId'],
                                        'requestId': message.get('requestId', ''),
                                        'taskMessageId': message.get('taskMessageId', ''),
                                        'serviceName': serviceName,
                                        'completed': True})
        else:
            next_step = pipeline[0]
            remaining = pipeline[1:]
            message['pipeline'] = remaining
            message['taskMessageId'] = str(uuid.uuid1())
            self.send_progress_message({'jobId': message['jobId'],
                                       'requestId': message.get('requestId', ''),
                                       'taskMessageId': message.get('taskMessageId', ''),
                                       'serviceName': serviceName,
                                       'queued': True})

            self.__channel.basic_publish(exchange='appMessagesExchange', routing_key=next_step, body=json.dumps(message)) 
 

    def send_progress_message(self, message):
        """Publishes a progress message to NSSI's progressExchange."""

        self.__channel.basic_publish(exchange='progressExchange', routing_key='progressQueue', body=json.dumps(message))

    def retry_message(self, attempt_num, method, properties, message):
        """Sends the given message to the next retry queue."""

        properties.headers['x-retried-count'] = attempt_num
        properties.headers['x-original-exchange'] = method.exchange
        properties.headers['x-original-routing-key'] = method.routing_key

        retry_queue = self.__retry_queues[attempt_num - 1] 
        self.__channel.basic_publish(exchange='', routing_key=retry_queue, properties=properties, body=json.dumps(message))

    def park_message(self, message):
        """Sends the given message to the parking lot queue."""
        self.__channel.basic_publish(exchange='', routing_key='parkingLotQueue', body=json.dumps(message))


class NSSIService:
    """A class implementing the RMQ protocol for NSSI data-processing services."""

    def __init__(self, name, queue, cancel_queue=None, retry_config=None):
        self.name = ''.join(filter(str.isalnum, name))
        self.queues = [{'queue': queue, 'callback': self.__wrap_callback(self.process_message)}]
        if cancel_queue:
            self.queues.append({'queue': cancel_queue, 'callback': self.__wrap_callback(self.process_cancellation)})

        self.retry = retry_config

    def __wrap_callback(self, callback):
        """Wraps the message-processing callback defined by the service. See __process_with_retry below."""

        return lambda ch, method, properties, message: self.__process_with_retry(method, properties, message, callback)

    def __process_with_retry(self, method, properties, message, callback):
        """Receives messages from NSSI and performs the following work:
             1. Sends NSSI a progress update to indicate the message is in-progress.
             2. Invokes the message-processing callback on the deserialized message.
             3. If the callback completes successfully, sends NSSI another progress update
                to indicate that the message processing is complete.
             4. If the callback raised an exception, determines whether the message can
                be retried, and if so, sends it to the appropriate retry queue. If not, 
                it sends the message to the parking lot queue, and updates NSSI of the failure."""

        service_message = json.loads(message)
        job_id = service_message['jobId']
        request_id = service_message.get('requestId', '')
        task_message_id = service_message.get('taskMessageId', '')

        rmqutils.send_progress_message({'jobId': job_id,
                                       'requestId': request_id,
                                       'taskMessageId': task_message_id,
                                       'serviceName': self.name,
                                       'inProgress': True})

        try:
            callback(service_message)
            rmqutils.send_progress_message({'jobId': job_id,
                                           'requestId': request_id,
                                           'taskMessageId': task_message_id,
                                           'serviceName': self.name,
                                           'completed': True})
        except Exception as e:
            retried_count = properties.headers.get('x-retried-count', 0)

            if self.retry and retried_count < self.retry.get('max_retries', 0):
                rmqutils.retry_message(retried_count + 1, method, properties, service_message)
            else:
                rmqutils.park_message(service_message)
                rmqutils.send_progress_message({'jobId': job_id,
                                               'requestId': request_id,
                                               'taskMessageId': task_message_id,
                                               'serviceName': self.name,
                                               'failed': True})

    def send_to_next_step(self, message):
        """Forwards the result message to the next processing step."""

        rmqutils.send_to_next_step(self.name, message)

    def process_message(self, message):
        raise NotImplementedError('Implement data-processing logic in this method.')

    def process_cancellation(self, message):
        pass


class NSSIConsumer():
    """A class to manage the queues needed for interacting with NSSI."""
  
    def __init__(self, services=[], rmq_params={}):
        rmqutils.connect(rmq_params)

        for service in services:
            if not service or not issubclass(type(service), NSSIService):
                # A NSSIService must be specified
                continue

            for queue in service.queues:
                rmqutils.declare_queue_and_listen(queue['queue'], queue['callback'])

            if service.retry:
                for i in range(service.retry.get('max_retries', 0)):
                    retry_queue = f'{service.name}Retry{i}'
                    wait_seconds = service.retry.get('retry_wait_seconds', 60.0)
                    backoff = service.retry.get('backoff', 1.5)
                    ttl_milliseconds = math.floor(wait_seconds * (backoff ** (i+1)) * 1000)

                    rmqutils.declare_retry_queue(retry_queue, ttl_milliseconds)

    def listen(self):
        rmqutils.listen()


rmqutils = RMQUtils()
