# NSSI Python Utilities

[[_TOC_]]

This package contains utility classes for developing data-processing services that can be plugged
into [NSSI](https://gitlab.com/calincs/conversion/NSSI).

## Installation
To install the package from a specific branch, run the following:

`python3 -m pip install git+https://gitlab.com/calincs/conversion/nssi_utils.git@[branch_name]#egg=nssi_utils`

for example:

`python3 -m pip install git+https://gitlab.com/calincs/conversion/nssi_utils.git@1-initial-draft#egg=nssi_utils`

## Usage <a name="usage"></a>
The `nssi_utils` package provides two classes: `NSSIConsumer` and `NSSIService`. The `NSSIConsumer`
class is responsible for setting up the queues and bindings needed for the service, while the
`NSSIService` class implements the RMQ protocol for interacting with NSSI, and should be subclassed
by your application to implement your own data-processing service.

### Example
```
from nssi_utils import NSSIService, NSSIConsumer
import pika, sys, os


# For each service you want to define, create a subclass of `NSSIService` that overrides the process_message
# method. This method takes in a parameter which is the deserialized payload of the RMQ message. Note that
# this method _must_ be implemented in your class, or the application will raise an exception.

class MyService(NSSIService):

  def process_message(self, message):
    """Takes in a JSON object, where the data to be processed is contained in the 'document'
       property, and uppercases all text in the document."""

    data = message['document']
    transformed_data = data.upper()
    message['document'] = transformed_data

    if message['jobId'] == 42:
      # If your service raises an error, it will be handled automatically by NSSIService, and either
      # routed to a retry queue if you have configured retries for your service, or sent to the
      # parking lot queue as a failure.

      raise Exception('An exception occurred')
    else:
      # If your service produces data for later steps in the pipeline, you _must_ call NSSIService's
      # send_to_next_step method to pass that data on to the next step.

      self.send_to_next_step(message) 

  # You can also optionally override the process_cancellation method, if your service handles cancellations.
  def process_cancellation(self, message):
    print(f'Oh no a cancellation! {message})


# Instantiate your data processing class(es) with the name of each service, the names of the service queue and
# (optionally) the cancellations queue, and the service's retry configuration (also optional).

my_service = MyService(name='myService',
                       queue='test',
                       cancel_queue='testCancellation',
                       retry_config={'max_retries': 2,
                                     'retry_wait_seconds': 10,
                                     'retry_backoff': 2.5})


# Instantiate NSSIConsumer, passing in all the services you have defined and the RMQ connection parameters.
# The example below shows the default values for the complete configuration. Leave out this config entirely
# if you want to use all default connection values, or pass in a dict with just the ones you want to override.

my_consumer = NSSIConsumer(services=[my_service],
                           rmq_params={'user': 'guest',
                                       'password': 'guest',
                                       'host': 'localhost',
                                       'port': 5672,
                                       'virtual_host': '/'})

 
# Finally, call the consumer object's listen method to start listening for messages from NSSI.

try:
  my_consumer.listen()
except KeyboardInterrupt:
  print('Interrupted')
  try:
    sys.exit(0)
  except SystemExit:
    os._exit(0)
```

## Deploying to NSSI

Once your service is implemented, you can plug it into NSSI by deploying it to the same cluster as the
instance of NSSI that you want to integrate with. It does not need to be deployed within the same namespace,
but it must be able to talk to RMQ. (Make sure that your RMQ connection details point to the correct RMQ
instance and virtual host.)

To allow your service to be used from the NSSI API, you will need to add a new job service definition, and
add or update a workflow definition in the [NSSI configuration](https://gitlab.com/calincs/conversion/NSSI/-/blob/main/Service/src/main/resources/application.properties#L36), as follows:

```
# Add the following lines under the 'Job Service settings' section:
nssi.processing.services.[YOUR_SERVICE_NAME].fullName=[Human-readable name of service]
nssi.processing.services.[YOUR_SERVICE_NAME].jobQueue=[queue name]
nssi.processing.services.[YOUR_SERVICE_NAME].cleanupQueue=[cancellations queue name] # optional
nssi.processing.services.[YOUR_SERVICE_NAME].uri=/api/services/[YOUR_SERVICE_NAME]

# Add or update the following lines under the 'Job Workflow settings' section for the workflow(s) which your new service will be part of:
nssi.processing.workflows.[YOUR_WORKFLOW_NAME].fullName=[Human-readable name of workflow]
nssi.processing.workflows.[YOUR_WORKFLOW_NAME].pipeline=[comma-separated, ordered list of services; add YOUR_SERVICE_NAME in this list where appropriate]
nssi.processing.workflows.[YOUR_WORKFLOW_NAME].resultsType=[either 'database', 's3', or 'elucidate', according to the final output type of this workflow]
```
