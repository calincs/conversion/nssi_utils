from setuptools import setup, find_packages

setup(name='nssi_utils',
      version='0.1',
      description='Utility packages for working with NSSI',
      url='todo',
      license='GPL2',
      packages=['nssi_utils'],
      zip_safe=False)
